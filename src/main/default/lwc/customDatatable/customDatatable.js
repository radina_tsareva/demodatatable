import LightningDatatable from 'lightning/datatable';
import inputType from './inputType.html';
import buttonType from './buttonType.html';
export default class customDatatable extends LightningDatatable {
    static customTypes = {
        inputType: {
            template: inputType,
            // Provide template data here if needed
            typeAttributes: [],
        },
        buttonType: {
            template: buttonType,
            // Provide template data here if needed
            typeAttributes: [],
        },

    };
}