/**
 * Created by Radi on 30.10.2019 г..
 */

import {LightningElement,track} from 'lwc';
// import { event } from 'c/pubsub';

export default class ButtonTypeComponent extends LightningElement {
    @track elements=['1'];
    handleClick(){
        this.elements.push('1');
        //use pubsub for comunication between siblings
        // https://github.com/trailheadapps/lwc-recipes/tree/master/force-app/main/default/lwc/pubsub

        // const ChangeEvent = new CustomEvent('buttonchange', {
        //     detail: this.elements.length ,
        // });
        // this.dispatchEvent(ChangeEvent);

        // let button =this.template.createElement('lightning-button-icon');
        // console.log('button',button);
        // button.setAttribute('iconName','utility:edit');
        // this.template.appendChild(button);

    }
}