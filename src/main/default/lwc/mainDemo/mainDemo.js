/**
 * Created by Radi on 30.10.2019 г..
 */

import { LightningElement, api, track, wire } from 'lwc';

export default class mainDemo extends LightningElement {

    @track data=[{product: 'single price', price: '10',Quantity: '10'}];
    @track columns = [
        {label: 'Quote product',
            fieldName: 'product',
            type: 'text',
        },
        {
            label: 'Tariff Price', //button for double
            fieldName: 'price',
            type: 'buttonType',
        },
        {
            label: 'Quantity',
            fieldName: 'Quantity',
            type: 'inputType',
            typeAttributes: {
            }
        }
    ];
}